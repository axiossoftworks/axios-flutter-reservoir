import 'package:example/Widgets/Styles/TextWidgetStyles.dart';
import 'package:example/Widgets/TextWidgets.dart';
import 'package:flutter/material.dart';

class ButtonWidget extends StatelessWidget {
  final String text;

  final Color textColor;

  final void Function() onPressed;

  ButtonWidget({@required this.text, @required this.onPressed, this.textColor});
  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: TextWidget(
        text,
        style: TWStyle.customtWStyle(
            color: textColor ?? Colors.black.withOpacity(0.5)),
      ),
      onPressed: onPressed,
    );
  }
}
