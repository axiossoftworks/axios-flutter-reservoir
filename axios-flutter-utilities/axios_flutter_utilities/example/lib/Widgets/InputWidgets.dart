import 'package:example/Widgets/Styles/TextWidgetStyles.dart';
import 'package:flutter/material.dart';

class TextFieldWidget extends StatefulWidget {
  @override
  _TextFieldWidgetState createState() => _TextFieldWidgetState();
}

class _TextFieldWidgetState extends State<TextFieldWidget> {
  var isSelected = true;
  Color getColor(bool isSelected) => (isSelected ?? false)
      ? (Colors.blue)
      : (Colors.grey[300].withOpacity(0.5));

//  var alignment=AlignmentTween(begin: Alignment.center,TextAlign.right);
  @override
  Widget build(BuildContext context) {
    return ListView(
        padding: EdgeInsets.all(0),
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [
          AnimatedContainer(
            alignment: Alignment.centerLeft,
            // foregroundDecoration: ,
            margin: EdgeInsets.symmetric(
                horizontal: isSelected ? 24 : 0, vertical: isSelected ? 10 : 0),
            padding: EdgeInsets.symmetric(
                horizontal: isSelected ? 8 : 40, vertical: isSelected ? 4 : 0),
            decoration: BoxDecoration(
                // gradient: isSelected
                //     ? LinearGradient(
                //         begin: Alignment.topLeft,
                //         end: Alignment.bottomRight,
                //         colors: [
                //             Colors.grey[200].withOpacity(0.6),
                //             Colors.yellow[800],
                //             Colors.yellow[800]
                //           ])
                //     : LinearGradient(colors: [Colors.white, Colors.white]),
                borderRadius: BorderRadius.circular(isSelected ? 8 : 0),
                color: isSelected ? Color(0xfff6f6f6) : Colors.grey[50],
                boxShadow: [
                  BoxShadow(
                      color: isSelected
                          ? Colors.purple[200].withOpacity(0.2)
                          : Colors.transparent,
                      blurRadius: 8,
                      spreadRadius: 0.6,
                      offset: Offset(4, 4))
                ]
                // shape: isSelected ? BoxShape.circle : BoxShape.rectangle
                ),
            duration: Duration(milliseconds: 500),
            curve: Curves.decelerate,
            child: TextField(
              // textAlign: TextAlign.right,
              // showCursor: true,
              autofocus: true,
              onSubmitted: (value) => setState(() {
                isSelected = false;
              }),
              onTap: () => setState(() {
                isSelected = true;
              }),
              style: TWStyle.defaulttWStyle,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(right: 8),
                hintText: "Grow old and die!",
                hintStyle: TWStyle.customtWStyle(color: Colors.grey[400]),
                fillColor: isSelected ? Color(0xfff6f6f6) : Colors.grey[50],
                filled: true,
                enabledBorder: OutlineInputBorder(

                    // borderRadius: BorderRadius.circular(5),
                    borderSide: BorderSide.none),
                focusedBorder: OutlineInputBorder(
                  // borderRadius: BorderRadius.circular(30),
                  borderSide: BorderSide(
                    color: Colors.transparent,
                  ),
                ),
              ),
            ),
          ),
          AnimatedContainer(
            alignment: Alignment.centerLeft,
            // foregroundDecoration: ,
            margin: EdgeInsets.symmetric(
                horizontal: isSelected ? 24 : 0, vertical: isSelected ? 10 : 0),
            padding: EdgeInsets.symmetric(
                horizontal: isSelected ? 8 : 40, vertical: isSelected ? 4 : 0),
            decoration: BoxDecoration(
                // gradient: isSelected
                //     ? LinearGradient(
                //         begin: Alignment.topLeft,
                //         end: Alignment.bottomRight,
                //         colors: [
                //             Colors.grey[200].withOpacity(0.6),
                //             Colors.yellow[800],
                //             Colors.yellow[800]
                //           ])
                //     : LinearGradient(colors: [Colors.white, Colors.white]),
                borderRadius: BorderRadius.circular(isSelected ? 8 : 0),
                color: isSelected ? Color(0xfff6f6f6) : Colors.grey[50],
                boxShadow: [
                  BoxShadow(
                      color: isSelected
                          ? Colors.purple[200].withOpacity(0.2)
                          : Colors.transparent,
                      blurRadius: 8,
                      spreadRadius: 0.6,
                      offset: Offset(4, 4))
                ]
                // shape: isSelected ? BoxShape.circle : BoxShape.rectangle
                ),
            duration: Duration(milliseconds: 500),
            curve: Curves.decelerate,
            child: TextField(
              // textAlign: TextAlign.right,
              // showCursor: true,
              autofocus: true,
              onSubmitted: (value) => setState(() {
                isSelected = false;
              }),
              onTap: () => setState(() {
                isSelected = true;
              }),
              style: TWStyle.defaulttWStyle,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(right: 8),
                hintText: "Grow old and die!",
                hintStyle: TWStyle.customtWStyle(color: Colors.grey[400]),
                fillColor: isSelected ? Color(0xfff6f6f6) : Colors.grey[50],
                filled: true,
                enabledBorder: OutlineInputBorder(

                    // borderRadius: BorderRadius.circular(5),
                    borderSide: BorderSide.none),
                focusedBorder: OutlineInputBorder(
                  // borderRadius: BorderRadius.circular(30),
                  borderSide: BorderSide(
                    color: Colors.transparent,
                  ),
                ),
              ),
            ),
          ),
          AnimatedContainer(
            alignment: Alignment.centerLeft,
            // foregroundDecoration: ,
            margin: EdgeInsets.symmetric(
                horizontal: isSelected ? 24 : 0, vertical: isSelected ? 10 : 0),
            padding: EdgeInsets.symmetric(
                horizontal: isSelected ? 8 : 40, vertical: isSelected ? 4 : 0),
            decoration: BoxDecoration(
                // gradient: isSelected
                //     ? LinearGradient(
                //         begin: Alignment.topLeft,
                //         end: Alignment.bottomRight,
                //         colors: [
                //             Colors.grey[200].withOpacity(0.6),
                //             Colors.yellow[800],
                //             Colors.yellow[800]
                //           ])
                //     : LinearGradient(colors: [Colors.white, Colors.white]),
                borderRadius: BorderRadius.circular(isSelected ? 8 : 0),
                color: isSelected ? Color(0xfff6f6f6) : Colors.grey[50],
                boxShadow: [
                  BoxShadow(
                      color: isSelected
                          ? Colors.purple[200].withOpacity(0.2)
                          : Colors.transparent,
                      blurRadius: 8,
                      spreadRadius: 0.6,
                      offset: Offset(4, 4))
                ]
                // shape: isSelected ? BoxShape.circle : BoxShape.rectangle
                ),
            duration: Duration(milliseconds: 500),
            curve: Curves.decelerate,
            child: TextField(
              textAlign: TextAlign.left,
              // showCursor: true,
              autofocus: true,
              onSubmitted: (value) => setState(() {
                isSelected = false;
              }),
              onTap: () => setState(() {
                isSelected = true;
              }),
              // fo
              maxLines: 5,

              style: TWStyle.defaulttWStyle,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(top: 8, right: 8),
                hintText: "Grow old and die!",
                fillColor: isSelected ? Color(0xfff6f6f6) : Colors.grey[50],
                filled: true,
                hintStyle: TWStyle.customtWStyle(color: Colors.grey[400]),
                enabledBorder: UnderlineInputBorder(

                    // borderRadius: dBorderRadius.circular(5),
                    borderSide: BorderSide(color: Colors.white)),
                focusedBorder: OutlineInputBorder(
                  // borderRadius: BorderRadius.circular(30),
                  borderSide: BorderSide(
                    color: Colors.transparent,
                  ),
                ),
              ),
            ),
          ),
          AnimatedContainer(
            alignment: Alignment.centerLeft,
            // foregroundDecoration: ,
            margin: EdgeInsets.symmetric(
                horizontal: isSelected ? 24 : 0, vertical: isSelected ? 10 : 0),
            padding: EdgeInsets.symmetric(
                horizontal: isSelected ? 8 : 40, vertical: isSelected ? 4 : 0),
            decoration: BoxDecoration(
                // gradient: isSelected
                //     ? LinearGradient(
                //         begin: Alignment.topLeft,
                //         end: Alignment.bottomRight,
                //         colors: [
                //             Colors.grey[200].withOpacity(0.6),
                //             Colors.yellow[800],
                //             Colors.yellow[800]
                //           ])
                //     : LinearGradient(colors: [Colors.white, Colors.white]),
                borderRadius: BorderRadius.circular(isSelected ? 8 : 0),
                color: isSelected ? Color(0xfff6f6f6) : Colors.grey[50],
                boxShadow: [
                  BoxShadow(
                      color: isSelected
                          ? Colors.purple[200].withOpacity(0.2)
                          : Colors.transparent,
                      blurRadius: 8,
                      spreadRadius: 0.6,
                      offset: Offset(4, 4))
                ]
                // shape: isSelected ? BoxShape.circle : BoxShape.rectangle
                ),
            duration: Duration(milliseconds: 500),
            curve: Curves.decelerate,
            child: TextField(
              textAlign: TextAlign.left,
              // showCursor: true,
              autofocus: true,
              onSubmitted: (value) => setState(() {
                isSelected = false;
              }),
              onTap: () => setState(() {
                isSelected = true;
              }),
              // fo
              maxLines: 5,

              style: TWStyle.defaulttWStyle,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(top: 8, right: 8),
                hintText: "Grow old and die!",
                fillColor: isSelected ? Color(0xfff6f6f6) : Colors.grey[50],
                filled: true,
                hintStyle: TWStyle.customtWStyle(color: Colors.grey[400]),
                enabledBorder: UnderlineInputBorder(

                    // borderRadius: dBorderRadius.circular(5),
                    borderSide: BorderSide(color: Colors.white)),
                focusedBorder: OutlineInputBorder(
                  // borderRadius: BorderRadius.circular(30),
                  borderSide: BorderSide(
                    color: Colors.transparent,
                  ),
                ),
              ),
            ),
          ),
        ]);
  }
}
