import 'package:axios_flutter_utilities/ConnectivityService/ConnectivityService.dart';
import 'package:flutter/cupertino.dart';

class ConsumerWidget<T> extends StatelessWidget {
  final Widget childWidget;

  ConsumerWidget({@required this.childWidget});

  @override
  Widget build(BuildContext context) {
    return Consumer<T>(
        builder: (BuildContext context, T model, Widget child) => childWidget);
  }
}
