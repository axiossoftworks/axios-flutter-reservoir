import 'dart:math';

import 'package:axios_flutter_utilities/ConnectivityService/ConnectivityService.dart';
import 'package:example/Widgets/Styles/TextWidgetStyles.dart';
import 'package:example/Widgets/TextWidgets.dart';
import 'package:example/main.dart';
import 'package:flutter/material.dart';

/** AnimatedC Short for my custom AnimatedContainer. Tweening margin, padding, color, boxshadow and borderRadius gives it an awesome selection effect */
class AnimatedC extends StatelessWidget {
  final bool isSelected;
  final Duration duration;
  final Widget child;
  final Color selectedColor;
  final Color unselectedColor;

  AnimatedC(
      {@required this.isSelected,
      this.duration,
      @required this.child,
      this.selectedColor,
      this.unselectedColor});

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
        margin: EdgeInsets.symmetric(
            horizontal: isSelected ? 16 : 0, vertical: isSelected ? 10 : 0),
        padding: EdgeInsets.symmetric(
            horizontal: isSelected ? 16 : 0, vertical: isSelected ? 4 : 0),
        duration: duration ?? Duration(milliseconds: 250),
        curve: Curves.decelerate,
        decoration: BoxDecoration(
            color: _getColor(isSelected).withOpacity(0.6),
            // gradient: isSelected
            //     ? LinearGradient(
            //         begin: Alignment.topLeft,
            //         end: Alignment.bottomRight,
            //         colors: [
            //             Colors.grey[200].withOpacity(0.6),
            //             Colors.yellow[800],
            //             Colors.yellow[800]
            //           ])
            //     : LinearGradient(colors: [Colors.white, Colors.white]),
            borderRadius: BorderRadius.circular(isSelected ? 8 : 0),
            boxShadow: [
              BoxShadow(
                  color: isSelected ? Colors.grey[400] : Colors.transparent,
                  blurRadius: 8,
                  spreadRadius: 1,
                  offset: Offset(2, 4))
            ]
            // shape: isSelected ? BoxShape.circle : BoxShape.rectangle
            ),
        child: child);
  }

  Color _getColor(bool isSelected,
          {Color selectedColor, Color unselectedColor}) =>
      (isSelected ?? false)
          ? (selectedColor ?? Colors.blue)
          : (unselectedColor ?? Colors.white);

  static Color _getTextColor(bool isSelected) =>
      !(isSelected ?? false) ? (Colors.black54) : (Colors.white);
}

class SelectableContainer extends StatefulWidget {
  final String text;
  final Duration duration;
  final Color selectedColor;
  final Color unselectedColor;
  final void Function(bool, String) onTap;
  Color color;

  SelectableContainer(
    this.text, {
    @required this.onTap,
    this.duration,
    this.selectedColor,
    this.unselectedColor,
  }) {
    color = unselectedColor;
  }

  @override
  _SelectableContainerState createState() => _SelectableContainerState();
}

class _SelectableContainerState extends State<SelectableContainer> {
  bool isSelected = false;
  Color color;

  void initState() {
    color = widget.color;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    isSelected =
        Provider.of<MultiSelectFormViewModel>(context).selected(widget.text);
    return AnimatedC(
      isSelected: isSelected,
      selectedColor: Colors.purple,
      unselectedColor: Colors.grey[100],
      child: GestureDetector(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: TextWidget(
                widget.text,
                style: TWStyle.customtWStyle(
                    color: AnimatedC._getTextColor(isSelected)),
              ),
            ),
          ],
        ),
        onTap: () {
          if (mounted)
            setState(() {
              isSelected = !isSelected;
            });
          widget.onTap(isSelected, widget.text);
        },
      ),
    );
  }
}
