import 'dart:math';

import 'package:example/Core/DataModels.dart';
import 'package:example/main.dart';
import 'package:flutter/cupertino.dart';

class FormSet {
  String name;
  Widget widget;
  List options;
  var inputValue;
  FormSet({String name, Widget widget, List options}) {
    this.name = name;
    this.options = options ?? exampleOptions;
    this.widget = widget;

    this.inputValue = initValue(widget.runtimeType);
  }

  initValue(Type widget) {
    switch (widget) {
      case MultiSelectFormPage:
        return [];
        break;
      default:
        return "";
    }
    return dataTypes.where((e) => e["model"] == widget).toList()[0]["type"];
  }
}
