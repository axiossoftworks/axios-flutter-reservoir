class SelectableModel {
  final String value;
  final bool selected;

  SelectableModel({this.value, this.selected = false});
}
