import 'package:axios_flutter_utilities/ConnectivityService/ConnectivityService.dart';
import 'package:example/Core/DataModels.dart';
import 'package:example/Core/Models/GodModel.dart';
import 'package:example/Widgets/ButtonWidgets.dart';
import 'package:example/Widgets/ContainerWidgets.dart';
import 'package:example/Widgets/InputWidgets.dart';
import 'package:example/Widgets/Styles/TextWidgetStyles.dart';
import 'package:example/Widgets/TextWidgets.dart';
import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(home: RouterPage()));

class RouterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
            Colors.blue.withOpacity(0.6),
            Colors.grey.withOpacity(0.5)
          ])),
      child: ChangeNotifierProvider<MultiSelectFormViewModel>(
          create: (BuildContext context) => MultiSelectFormViewModel(),
          child: Consumer<MultiSelectFormViewModel>(
              builder: (BuildContext context, MultiSelectFormViewModel model,
                      Widget child) =>
                  AnimatedSwitcher(
                    switchInCurve: Curves.decelerate,
                    switchOutCurve: Curves.decelerate,
                    transitionBuilder: (child, animation) =>
                        ScaleTransition(scale: animation, child: child),
                    duration: Duration(milliseconds: 100),
                    reverseDuration: Duration(milliseconds: 100),
                    child: Scaffold(
                        key: ValueKey(model.step),
                        body: model.formSet[model.step].widget),
                  ))),
    );
  }
}

class InputFormPage extends StatefulWidget {
  final List list;
  InputFormPage({this.list});
  @override
  _InputFormPageState createState() => _InputFormPageState();
}

class _InputFormPageState extends State<InputFormPage>
    with TickerProviderStateMixin {
  ConnectivityService _connectivity = ConnectivityService.instance;

  var _source;

  AnimationController animationController;

  Animation<double> animation;

  MultiSelectFormViewModel model;

  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 2));
    animation = Tween<double>(begin: 0, end: 100).animate(animationController);

    // _connectivity.initialise();
    // _connectivity.myStream.listen((source) {
    //   setState(() => _source = source);
    // })
  }

  @override
  Widget build(BuildContext context) {
    // return Center(child: InputWidget());
    model = Provider.of<MultiSelectFormViewModel>(context);

    return Scaffold(
        // appBar: AppBar(title: Text("Internet")),
        body: Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextWidget("Write a poem:",
              style: TWStyle.customtWStyle(
                  fontSize: 24, color: Colors.blue[800].withOpacity(0.6))),
          TextFieldWidget(),
          model.formSet[model.step].inputValue.length == 0
              ? Row(
                  children: [
                    Expanded(
                        child: ButtonWidget(
                            text: "Back", onPressed: () => model.previous())),
                    Expanded(child: Container())
                  ],
                )
              : Row(
                  children: [
                    Expanded(
                        child: ButtonWidget(
                            text: "Back", onPressed: () => model.previous())),
                    Expanded(
                        child: model.end
                            ? ButtonWidget(
                                text: "Submit",
                                onPressed: () => model.handlePostData())
                            : ButtonWidget(
                                text: "Next", onPressed: () => model.next()))
                  ],
                )
        ],
      ),
    ));
  }

  pageRoute() {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) =>
          MultiSelectFormPage(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween =
            Tween<double>(begin: 0, end: 100).chain(CurveTween(curve: curve));

        return ScaleTransition(
          scale: animation,
          child: child,
        );
      },
    );
  }

  @override
  void dispose() {
    _connectivity.disposeStream();
    super.dispose();
  }
}

class MultiSelectFormPage extends StatefulWidget {
  final List list;
  MultiSelectFormPage({this.list});
  @override
  _MultiSelectFormPageState createState() => _MultiSelectFormPageState();
}

class _MultiSelectFormPageState extends State<MultiSelectFormPage>
    with TickerProviderStateMixin {
  ConnectivityService _connectivity = ConnectivityService.instance;

  var _source;

  AnimationController animationController;

  Animation<double> animation;

  MultiSelectFormViewModel model;

  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 2));
    animation = Tween<double>(begin: 0, end: 100).animate(animationController);

    // _connectivity.initialise();
    // _connectivity.myStream.listen((source) {
    //   setState(() => _source = source);
    // })
  }

  @override
  Widget build(BuildContext context) {
    // return Center(child: InputWidget());
    model = Provider.of<MultiSelectFormViewModel>(context);

    return Scaffold(
        // appBar: AppBar(title: Text("Internet")),
        body: Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextWidget("Select an option for your life:",
              style: TWStyle.customtWStyle(
                  fontSize: 24, color: Colors.blue[800].withOpacity(0.6))),
          // TextFieldWidget(),
          // TextFieldWidget(),
          MultipleChoiceWidget(),
          model.formSet[model.step].inputValue.length == 0
              ? Row(
                  children: [
                    Expanded(
                        child: ButtonWidget(
                            text: "Back", onPressed: () => model.previous())),
                    Expanded(child: Container())
                  ],
                )
              : Row(
                  children: [
                    Expanded(
                        child: ButtonWidget(
                            text: "Back", onPressed: () => model.previous())),
                    Expanded(
                        child: model.end
                            ? ButtonWidget(
                                text: "Submit",
                                onPressed: () => model.handlePostData())
                            : ButtonWidget(
                                text: "Next", onPressed: () => model.next()))
                  ],
                )
        ],
      ),
    ));
  }

  pageRoute() {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) =>
          MultiSelectFormPage(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween =
            Tween<double>(begin: 0, end: 100).chain(CurveTween(curve: curve));

        return ScaleTransition(
          scale: animation,
          child: child,
        );
      },
    );
  }

  @override
  void dispose() {
    _connectivity.disposeStream();
    super.dispose();
  }
}

class MultipleChoiceWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final model = Provider.of<MultiSelectFormViewModel>(context);
    return ListView(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      children: (model.formSet[model.step].options)
          .map((e) => SelectableContainer(
                e,
                selectedColor: Colors.blue,
                onTap: (bool isSelected, String item) =>
                    model.onItemTapped(isSelected, item),
              ))
          .toList(),
    );
  }
}

class MultiSelectFormViewModel extends ChangeNotifier {
  int step = 0;
  bool get end => (step >= formSet.length - 1);
  bool get start => (step <= 0);

  Map<String, dynamic> formStore = {};

  Map<String, dynamic> initializeFormStore() {
    this
        .formSet
        .map((e) => formStore.addEntries({e.name: e.inputValue}.entries))
        .toList();
    return formStore;
  }

  List<FormSet> formSet = [
    FormSet(
      name: "first",
      options: [
        "First choice 1",
        "First choice 2",
        "First choice 3",
        "First choice 4"
      ],
      widget: MultiSelectFormPage(),
    ),
    FormSet(
      name: "Input",
      widget: InputFormPage(),
    ),
    FormSet(
      name: "Second",
      options: [
        "Second choice 1",
        "Second choice 2",
        "Second choice 3",
        "Second choice 4"
      ],
      widget: MultiSelectFormPage(),
    ),
  ];

  next() {
    if (!end) {
      step++;
      notifyListeners();
    }
  }

  previous() {
    if (!start) {
      step--;
      notifyListeners();
    }
  }

  handlePostData() {
    initializeFormStore();
    print(initializeFormStore());
  }

  void onItemTapped(bool selected, String value) {
    List temp = formSet[step].inputValue;
    selected ? temp.add(value) : temp.remove(value);
    formSet[step].inputValue = temp;
    notifyListeners();
  }

  bool selected(text) => formSet[step].inputValue.contains(text);

  void toggleSelected(bool selected) => selected = !selected;

  static initValue(Type type) {
    print(dataTypes.where((e) => e["model"] == type).toList()[0]["type"]);
    return dataTypes.where((e) => e["model"] == type).toList()[0]["type"];
  }
}

class MultiFormData {
  final List selectedItems;
  MultiFormData({this.selectedItems});
}
