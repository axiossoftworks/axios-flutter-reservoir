class DateService {
  static String getMonth({required int month}) {
    List<String> _months = [
      "JANUARY",
      "FEBRUARY",
      "MARCH",
      "APRIL",
      "MAY",
      "JUNE",
      "JULY",
      "AUGUST",
      "SEPTEMBER",
      "OCTOBER",
      "NOVEMBER",
      "DECEMBER",
    ];
    return _months[month - 1];
  }

  static String getFormattedDate(DateTime date) {
    //returns date in "JAN 12, 2020"
    int month = date.month;
    int year = date.year;
    int day = date.day;
    return "$month $day, $year";
  }
}
