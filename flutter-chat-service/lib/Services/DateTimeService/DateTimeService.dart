import './DateService.dart';

class DateTimeService {
  static String dateDifference(
      {required DateTime startDate, required DateTime endDate}) {
    Duration difference = endDate.difference(startDate);
    int days = difference.inDays;
    if (days < 1) {
      int secs = difference.inSeconds;
      if (secs < 60) {
        return "$secs seconds ago";
      } else if (secs < 3600) {
        int mins = difference.inMinutes;
        return "$mins minutes ago";
      } else {
        int hours = difference.inHours;
        return "$hours hours ago";
      }
    } else if (days < 7) {
      return "$days days ago";
    } else {
      return DateService.getFormattedDate(startDate);
    }
  }
}
