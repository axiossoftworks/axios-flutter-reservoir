class ApiResponse {
  // Class for managing ApiResponse
  late int _status;
  late String? _error;
  var _data;

  ApiResponse({required int status, String? error, data}) {
    _error = error;
    _status = status;
    _data = data;
    print(_error); // remove it
  }
  int get status => _status;
  String get errorMsg => _status == 0 ? "No Connection!!!" : "Unknown Error";
  dynamic get data => _data;
}
