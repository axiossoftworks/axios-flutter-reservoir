library chat_service;

import 'package:chat_service/Core/Models/SingleSenderDetailModel.dart';

import 'BaseViewModel.dart';

class ChatListService extends BaseViewModel {
  ChatListService({required url}) : super(url: url);
  @override
  otherFunction(response, List listData) {
    return response.data.map((item) {
      return SingleSenderDetailModel.fromJson(item);
    }).toList();
  }
}
