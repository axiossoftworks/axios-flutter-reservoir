import 'package:json_annotation/json_annotation.dart';

part 'SingleSenderDetailModel.g.dart';

@JsonSerializable()
class SingleSenderDetailModel {
  final int? sender;
  final String? sendername;
  final String? subject;
  final String? detail;
  final String? created_at;
  final dynamic imgarray;

  SingleSenderDetailModel({
    this.sender,
    this.sendername,
    this.subject,
    this.detail,
    this.created_at,
    this.imgarray,
  });
  factory SingleSenderDetailModel.fromJson(Map<String, dynamic> json) =>
      _$SingleSenderDetailModelFromJson(json);
  Map<String, dynamic> toJson() => _$SingleSenderDetailModelToJson(this);
}
