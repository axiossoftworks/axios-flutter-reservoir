import 'package:json_annotation/json_annotation.dart';
part 'SingleChatMessageModel.g.dart';

@JsonSerializable()
class SingleChatMessageModel {
  late final bool isReceived;
  late final String detail;
  late final String createdAt;

  SingleChatMessageModel({
    required this.isReceived,
    required this.detail,
    required this.createdAt,
  });
  factory SingleChatMessageModel.fromJson(Map<String, dynamic> json) =>
      _$SingleChatMessageModelFromJson(json);
  Map<String, dynamic> toJson() => _$SingleChatMessageModelToJson(this);
}
