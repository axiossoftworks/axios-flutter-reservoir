// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'SingleSenderDetailModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SingleSenderDetailModel _$SingleSenderDetailModelFromJson(
    Map<String, dynamic> json) {
  return SingleSenderDetailModel(
    sendername: json['sendername'] ?? "",
    // image: json['imgarray'] as String,
    sender: json['sender'] ?? -1,
    subject: json['subject'] ?? "",
    detail: json['detail'] ?? "",
    created_at: json['created_at'] ?? "",
    imgarray: json['imgarray'] ?? ["https://i.stack.imgur.com/l60Hf.png"],
  );
}

Map<String, dynamic> _$SingleSenderDetailModelToJson(
        SingleSenderDetailModel instance) =>
    <String, dynamic>{
      'sendername': instance.sendername,
      // 'image': instance.image,
      'sender': instance.sender,
      'subject': instance.subject,
      'detail': instance.detail,
      'createdAt': instance.created_at,
      'imgarray': instance.imgarray,
    };
