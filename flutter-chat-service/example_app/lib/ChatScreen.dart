import 'dart:math';

import 'package:chat_service/Core/ViewModel/ChatViewModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';

import 'ChatBubble.dart';

enum MessageType {
  Sender,
  Receiver,
}

class ChatScreen extends StatefulWidget {
  final String senderId;
  ChatScreen({required this.senderId});
  @override
  _ChatScreenState createState() => _ChatScreenState(senderId);
}

class _ChatScreenState extends State<ChatScreen> {
  String senderId;
  _ChatScreenState(this.senderId);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ChatViewModel>(
      create: (_) => ChatViewModel(senderId: senderId, url: "abc"),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: chatAppBar(),
        body: Column(
          children: [
            Expanded(
              child: Consumer<ChatViewModel>(
                builder: (BuildContext context, ChatViewModel chatViewModel,
                        child) =>
                    chatViewModel.getData == null
                        ? CircularProgressIndicator()
                        : Container(
                            child: ListView.builder(
                                reverse: true,
                                shrinkWrap: true,
                                itemCount: chatViewModel.getData.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return ChatBubble(
                                    singleChatModel:
                                        chatViewModel.getData[index],
                                  );
                                }),
                          ),
              ),
            ),
            bottomWriteMessage(),
          ],
        ),
      ),
    );
  }

  AppBar chatAppBar() {
    return AppBar(
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      leading: IconButton(
        icon: Icon(
          Boxicons.bxs_chevron_left,
          color: Colors.black,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      title: Row(
        children: [
          CircleAvatar(
            backgroundImage: AssetImage('Assets/images/image1.jpeg'),
          ),
          SizedBox(width: 10.0),
          Text(
            'Shiwani Shrestha',
            style: TextStyle(color: Colors.black, fontFamily: 'Poppins'),
          ),
        ],
      ),
    );
  }

  Widget bottomWriteMessage() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        margin: EdgeInsets.only(top: 8.0),
        padding: EdgeInsets.only(
          left: 24.0,
          bottom: 8.0,
        ),
        width: double.infinity,
        color: Colors.white,
        child: Row(
          children: [
            Expanded(
              child: TextField(
                decoration: InputDecoration(
                    hintText: "Write a message...",
                    hintStyle: TextStyle(color: Colors.black54),
                    border: InputBorder.none),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            IconButton(
              icon: Icon(
                Boxicons.bx_image_alt,
                color: Colors.grey,
              ),
              onPressed: () {},
            ),
            sendButton(),
          ],
        ),
      ),
    );
  }

  sendButton() {
    return Container(
      margin: EdgeInsets.only(top: 8.0, bottom: 3.0, right: 8.0),
      decoration: BoxDecoration(
          gradient: RadialGradient(
            center: const Alignment(0.5, -0.5),
            radius: 0.5,
            colors: [Colors.blueAccent, Colors.blue],
          ),
          color: Colors.blue,
          borderRadius: BorderRadius.circular(10.0)),
      child: Transform.rotate(
        angle: 180 * pi / 100,
        child: IconButton(
          icon: Icon(
            Boxicons.bx_send,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
      ),
    );
  }
}
