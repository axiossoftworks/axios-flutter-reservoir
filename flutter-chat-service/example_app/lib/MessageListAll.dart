import 'package:chat_service/Core/ViewModel/ChatListService.dart';
import 'package:flutter/material.dart';
import './Message.dart';
import 'package:provider/provider.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:animated_text_kit/animated_text_kit.dart';

class MessageListAll extends StatefulWidget {
  @override
  _MessageListAllState createState() => _MessageListAllState();
}

class _MessageListAllState extends State<MessageListAll> {
  bool selected = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.transparent,
      body: ChangeNotifierProvider<ChatListService>(
        create: (_) =>
            ChatListService(url: "https://trial.nivid.app/listMessages"),
        child: CustomScrollView(
          slivers: [
            SliverPadding(
              padding: EdgeInsets.only(top: 48, left: 24.0, right: 24.0),
              sliver: SliverToBoxAdapter(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Message',
                    ),
                    CircleAvatar(),
                  ],
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: searchMessage(),
            ),
            Consumer<ChatListService>(
              builder: (BuildContext context, ChatListService chatListService,
                      child) =>
                  SliverList(
                delegate: SliverChildBuilderDelegate((context, index) {
                  return Message(
                    senderId: chatListService.getData[index].sender.toString(),
                    text: chatListService.getData[index].sendername,
                    secondaryText: chatListService.getData[index].detail,
                    image: (chatListService.getData[index].imgarray != null)
                        ? chatListService.getData[index].imgarray[0]
                        : "https://www.worldfuturecouncil.org/wp-content/uploads/2020/02/dummy-profile-pic-300x300-1.png", // default image rakhdeko
                    time: chatListService.getData[index].created_at,
                    isMessageRead: (index == 0 || index == 3) ? true : false,
                  );
                }, childCount: chatListService.getData.length),
              ),
            ),
          ],
        ),
      ),
    );
  }

  searchMessage() {
    return AnimatedContainer(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(selected ? 8 : 0),
          boxShadow: [
            BoxShadow(
                color: !selected
                    ? Colors.transparent
                    : Colors.grey.withOpacity(0.2),
                offset: Offset(4, 4),
                blurRadius: selected ? 10 : 0,
                spreadRadius: selected ? 0.2 : 0)
          ],
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: !selected
                  ? [
                      Color(0xFFffffff),
                      Color(0xFFffffff),
                    ]
                  : [
                      Color(0xFFffffff),
                      Color(0xFFffffff),
                    ])),
      margin: EdgeInsets.symmetric(
          horizontal: selected ? 20.0 : 0, vertical: selected ? 10 : 8),
      width: selected ? 140.h : 30.h,
      duration: Duration(milliseconds: 700),
      curve: Curves.decelerate,
      child: TextLiquidFill(
        text: 'LIQUIDY',
        waveColor: Colors.blueAccent,
        // boxBackgroundColor: Colors.white,
        // textStyle: TextStyle(
        //   fontSize: 20.0,
        //   fontWeight: FontWeight.bold,
        // ),
      ),
      // TextField(
      //   onTap: () => setState(() {
      //     selected = true;
      //   }),
      //   onSubmitted: (s) => setState(() {
      //     selected = false;
      //   }),
      //   textAlign: TextAlign.right,
      //   style: TextStyle(color: Colors.black54),
      //   // obscureText: true,
      //   decoration: InputDecoration(
      //     contentPadding: EdgeInsets.all(selected ? 8 : 4),
      //     suffixIcon: Icon(Icons.search),
      //     hintText: 'Search Message',
      //     hintStyle: TextStyle(
      //         fontFamily: 'Poppins', color: Colors.grey, fontSize: 48.sp),
      //     fillColor: Colors.transparent,
      //     // filled: true,
      //     enabledBorder: OutlineInputBorder(
      //       // borderRadius: BorderRadius.circular(10),
      //       borderSide: BorderSide(color: Colors.transparent),
      //     ),
      //     focusedBorder: OutlineInputBorder(
      //       borderRadius: BorderRadius.circular(32),
      //       borderSide: BorderSide(color: Colors.transparent),
      //     ),
      //   ),
      // ),
    );
  }
}
