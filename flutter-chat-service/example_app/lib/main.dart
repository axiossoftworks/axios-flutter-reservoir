import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'MessageListAll.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(1080, 2356),
      builder: () => MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(),
        home: MessageListAll(),
      ),
    );
  }
}
