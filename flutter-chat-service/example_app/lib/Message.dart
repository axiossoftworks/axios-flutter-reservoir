import 'package:chat_service/Services/DateTimeService/DateTimeService.dart';
import 'package:flutter/material.dart';

import 'ChatScreen.dart';

class Message extends StatefulWidget {
  final String senderId;
  final String text;
  final String secondaryText;
  final String image;
  final String time;
  final bool isMessageRead;

  Message({
    required this.senderId,
    required this.text,
    required this.secondaryText,
    required this.image,
    required this.time,
    required this.isMessageRead,
  });

  @override
  _MessageState createState() => _MessageState();
}

class _MessageState extends State<Message> {
  bool selected = false;

  Color? color;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(selected ? 16 : 0),
            gradient: LinearGradient(
              colors: !selected
                  ? [Colors.white, Colors.white]
                  : [
                      Colors.green[800]!.withOpacity(0.5),
                      Colors.blue[400]!.withOpacity(0.6),
                      Colors.blue[800]!.withOpacity(0.6)
                    ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
            boxShadow: [
              BoxShadow(
                  offset: Offset(4, 4),
                  color: selected ? color ?? Colors.grey[200]! : Colors.white,
                  blurRadius: selected ? 8 : 0.1,
                  spreadRadius: selected ? 4 : 0.1)
            ]),
        margin: EdgeInsets.all(selected ? 16 : 0),
        duration: Duration(milliseconds: 200),
        curve: Curves.decelerate,
        child: GestureDetector(
          onHorizontalDragStart: (d) => setState(() {
            selected = true;
            color = Colors.purple;
          }),
          onLongPress: () => setState(() {
            selected = true;
          }),
          onLongPressUp: () => setState(() {
            selected = false;
          }),
          onHorizontalDragEnd: (s) => setState(() {
            selected = false;
            color = Colors.yellow;
          }),
          child: ListTile(
            focusColor: Colors.red,
            leading: CircleAvatar(
              radius: 30.0,
              backgroundImage: NetworkImage(widget.image),
            ),
            title: Text(
              widget.text,
              style: TextStyle(color: selected ? Colors.white : Colors.black),
            ),
            subtitle: Text(
              widget.secondaryText,
              style: TextStyle(
                  color: selected ? Colors.white : Colors.black,
                  fontWeight: !selected ? FontWeight.normal : FontWeight.bold),
            ),
            trailing: Text(
              DateTimeService.dateDifference(
                startDate: DateTime.parse(widget.time),
                endDate: DateTime.now(),
              ),
            ),
            // onTap: () {
            //   Navigator.push(context, MaterialPageRoute(builder: (context) {
            //     return ChatScreen(senderId: widget.senderId);
            //   }));
            // }),
          ),
        ));
  }
}
