import 'package:chat_service/Core/Models/SingleChatMessageModel.dart';
import 'package:chat_service/Services/DateTimeService/DateTimeService.dart';
import 'package:flutter/material.dart';

class ChatBubble extends StatefulWidget {
  final SingleChatMessageModel singleChatModel;
  ChatBubble({required this.singleChatModel});
  @override
  _ChatBubbleState createState() => _ChatBubbleState();
}

class _ChatBubbleState extends State<ChatBubble> {
  late SingleChatMessageModel singleChatModel;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: (widget.singleChatModel.isReceived
          ? Alignment.topLeft
          : Alignment.topRight),
      child: Container(
        width: 600, // milauna baaki xa
        padding: EdgeInsets.only(
          left: 20,
          top: 10,
          bottom: 8.0,
        ),
        child: Stack(
          overflow: Overflow.visible,
          children: [
            messageContainer(),
            !widget.singleChatModel.isReceived
                ? Container()
                : Positioned(
                    left: -24.0,
                    bottom: -5.0,
                    // right: 10.0,
                    child: Container(
                      margin: const EdgeInsets.only(left: 8.0, right: 8.0),
                      child: CircleAvatar(
                        radius: 12.0,
                      ),
                    ),
                  )
          ],
        ),
      ),
    );
  }

  messageContainer() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16.0),
      decoration: BoxDecoration(
        borderRadius: !widget.singleChatModel.isReceived
            ? BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0),
                bottomLeft: Radius.circular(10.0))
            : BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0)),
        color: (!widget.singleChatModel.isReceived
            ? Colors.blue
            : Colors.grey.shade200),
      ),
      padding: EdgeInsets.only(top: 8.0, bottom: 16.0, left: 8.0, right: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(widget.singleChatModel.detail,
              style: !widget.singleChatModel.isReceived
                  ? TextStyle(fontSize: 15, color: Colors.white)
                  : TextStyle(fontSize: 15, color: Colors.black)),

          // TextStyle(fontSize: 15, color: Colors.white),
          // ),

          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Row(
              children: [
                SizedBox(width: 24.0),
                Flexible(
                  child: Text(
                      DateTimeService.dateDifference(
                        startDate:
                            DateTime.parse(widget.singleChatModel.createdAt),
                        endDate: DateTime.now(),
                      ),
                      style: !widget.singleChatModel.isReceived
                          ? TextStyle(fontSize: 10, color: Colors.white)
                          : TextStyle(fontSize: 10, color: Colors.grey)),
                ),
                // SizedBox(width: 10.w),
                // Text(createdAt(widget.singleChatModel.created_at),
                //     style: !widget.singleChatModel.isReceived
                //         ? TextStyle(fontSize: 10, color: Colors.white)
                //         : TextStyle(fontSize: 10, color: darkGreyColor)),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
