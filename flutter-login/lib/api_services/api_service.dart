import 'package:dio/dio.dart';
// import 'package:dio_http_cache/dio_http_cache.dart';

class ApiRequests {
  final Dio _dio = new Dio();
  ApiRequests({String jwtAuth}) {
    if (jwtAuth != null) {
      _dio.options.headers = {
        "content-type": "application/json",
        "Authorization": "Bearer $jwtAuth",
      };
    }
  }

  getRequest(String url) async {
    var response;

    try {
      response = await _dio.get(url);
      return response;
    } on DioError catch (e) {
      return {
        "status": 0,
        "error": e?.response?.statusMessage,
      };
    } catch (e) {
      print(e);
      return null;
    }
  }

  postRequest(String url, Map data) async {
    try {
      Response response = await _dio.post(url, data: data);
      return response;
    } on DioError catch (e) {
      return {
        "status": 0,
        "error": e?.response?.statusMessage,
      };
    } catch (e) {
      print(e);
      return null;
    }
  }

  // getRequestHandlerWithCache(String url, {Map<String, dynamic> data}) async {
  //   DioCacheManager _dioCacheManager = DioCacheManager(CacheConfig());
  //   _dio.interceptors.add(_dioCacheManager.interceptor);
  //   Options _cacheOptions = buildCacheOptions(
  //     Duration(days: 1),
  //     forceRefresh: true,
  //   );
  //   try {
  //     Response response = await _dio.get(url, options: _cacheOptions);
  //     return response;
  //   } on DioError catch (e) {
  //     return {"error": e.response.statusMessage};
  //   } catch (e) {
  //     print(e);
  //     return null;
  //   }
  // }

  putRequest(String url, var data) async {
    try {
      Response response = await _dio.put(url, data: data);
      return response;
    } on DioError catch (e) {
      return {
        "status": 0,
        "error": e?.response?.statusMessage,
      };
    } catch (e) {
      print(e);
      return null;
    }
  }
}
